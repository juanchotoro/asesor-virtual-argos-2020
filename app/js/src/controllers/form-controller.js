(function() {
    'use strict';

    var app = angular.module(window.appName);
    var controlador = function($scope, firebaseService) {

        // Esto es para hacer un random
        function shuffle(array) {
            var currentIndex = array.length,
                temporaryValue, randomIndex;
            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        }

        $scope.registrar = function(usr, form) {
            if (form.$valid) {
                $scope.getNumber = function(num) {
                    return new Array(num);
                }
                $scope.currentDate = new Date();
                var tipo = '';
                switch (usr.cargo) {
                    case 'Ferretero':
                        tipo = 'Masivo';
                        break;
                    case 'MaestroDo':
                        if (usr.producto_interes == 'Agregados') {
                            tipo = 'Masivo';
                        } else {
                            tipo = 'Industrial';
                        }
                        break;
                    default:
                        if (usr.producto_interes == 'Agregados') {
                            tipo = 'Agregados';
                        } else {
                            tipo = 'Industrial';
                        }
                        break;
                }

                firebaseService.obtenerAsesor(usr.departamento).$loaded()
                    .then(function(res) {
                        $scope.asesor = res.filter(function(asesor) {
                            return asesor.Activo == true && asesor.tipo == tipo;
                        });

                        if ($scope.asesor.length == 1) {
                            $scope.asesor = $scope.asesor[0];
                        } else {
                            $scope.asesor = shuffle($scope.asesor)[0];
                        }

                        firebaseService.crear(usr)
                            .then(function() {
                                var usrAsesor = {
                                    from: {
                                        name: "Argos",
                                        email: "no-reply@feelingcompany.org"
                                    },
                                    to: {
                                        name: $scope.asesor.Nombre,
                                        email: $scope.asesor.Email
                                    },
                                    templateId: 833354, //template del asesor
                                    subject: "Asesor comercial - Nueva Solicitud",
                                    data: {
                                        nombre: usr.nombre,
                                        apellido: usr.apellido,
                                        email: usr.email,
                                        celular: usr.telefono,
                                        ciudad: usr.ciudad,
                                        oficio: usr.cargo,
                                        producto: usr.producto_interes
                                    }
                                }
                                firebaseService.enviarEmail(usrAsesor);

                                var usrUsuario = {
                                    from: {
                                        name: "Argos",
                                        email: "no-reply@feelingcompany.org"
                                    },
                                    to: {
                                        name: usr.nombre,
                                        email: usr.email
                                    },
                                    templateId: 833360, //template del usuario
                                    subject: "Argos Expoconstrucción y Expodiseño 2019 - Asesor comercial",
                                    data: {
                                        nombre_usuario: usr.nombre,
                                        nombre_asesor: $scope.asesor.Nombre,
                                        email_asesor: $scope.asesor.Email,
                                        celular_asesor: $scope.asesor.Celular,
                                        ciudad_asesor: $scope.asesor.Ciudad
                                    }
                                }
                                firebaseService.enviarEmail(usrUsuario);

                                /// integracion pardot
                                var usrUsuario = {
                                    email: usr.email,
                                    data: 'first_name=' + usr.nombre + '&last_name=' + usr.apellido + '&phone=' + usr.telefono + '&city=' + usr.ciudad + '&state=' + usr.departamento + '&Custom_rea_funcional' + usr.area_funcional + '&company=' + usr.empresa + '&Profesi_n=' + usr.cargo + '&Tiempo_de_inicio_de_proyecto=' + usr.tiempo_inicio_proyecto + '&Tipo_de_proyecto=' + usr.producto_interes

                                }

                                firebaseService.pardot(usrUsuario);

                                $('#disclaimer').modal('show');
                            });
                    });
            } else {
                return false;
            }
        }

        $('#disclaimer').on('hidden.bs.modal', function(e) {
            $scope.$apply(function() {
                $scope.usr = {};
                $scope.asesor = {};
            })
        });
        $(document).ready(function() {
            $("#mapsvg").mapSvg({
                width: 612.82184,
                height: 694,
                colors: {
                    baseDefault: "#000000",
                    background: "rgba(238,238,238,0)",
                    selected: 40,
                    hover: 20,
                    directory: "#fafafa",
                    status: {},
                    base: "#16216a",
                    disabled: "#bbb",
                    stroke: "#ffffff"
                },
                viewBox: [0, -0.15982000000002472,
                    612.82184,
                    694
                ],
                cursor: "pointer",
                tooltips: {
                    mode: "title",
                    on: false,
                    priority: "local",
                    position: "bottom-right"
                },
                gauge: {
                    on: false,
                    labels: {
                        low: "low",
                        high: "high"
                    },
                    colors: {
                        lowRGB: {
                            r: 85,
                            g: 0,
                            b: 0,
                            a: 1
                        },
                        highRGB: {
                            r: 238,
                            g: 0,
                            b: 0,
                            a: 1
                        },
                        low: "#550000",
                        high: "#ee0000",
                        diffRGB: {
                            r: 153,
                            g: 0,
                            b: 0,
                            a: 0
                        }
                    },
                    min: 0,
                    max: false
                },
                source: "images/colombia.svg",
                title: "Colombia",
                responsive: true
            });
        });

    }
    controlador.$inject = ['$scope',
        'firebaseService'
    ];
    app.controller("form-controller", controlador);

})();