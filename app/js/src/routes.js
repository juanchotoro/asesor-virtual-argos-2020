(function(angular, appName) {
    'use strict';
    var app = angular.module(window.appName);

    function routingConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state({
                name: 'form',
                url: '/',
                templateUrl: 'views/form.html',
                controller: 'form-controller'
            })
            .state({
                name: 'thankyou',
                url: '/thankyou',
                templateUrl: 'views/thankyou.html'
            })

        $urlRouterProvider.otherwise('/');

    }
    routingConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    app.config(routingConfig);

})(window.angular, window.appName);