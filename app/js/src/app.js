(function(angular) {
    'use strict';
    if (!angular) { throw 'Angular no esta definido'; }

    window.appName = "asesor-virtual";

    var config = function() {
        if (firebase) {
            var config = {
                apiKey: "AIzaSyDDgALbh1K_IsrLSFYAhmhtyfMnyhuoJnQ",
                authDomain: "asesor-virtual-argos.firebaseapp.com",
                databaseURL: "https://asesor-virtual-argos.firebaseio.com",
                projectId: "asesor-virtual-argos",
                storageBucket: "asesor-virtual-argos.appspot.com",
                messagingSenderId: "895708267129",
                appId: "1:895708267129:web:afeaa32ba14925fc"
            };
            firebase.initializeApp(config);
        } else {
            throw 'firebase no esta definido';
        };

        $('.carousel').carousel()

    }
    config.$inject = [];

    angular.module(window.appName, ['ui.router', 'firebase', '720kb.datepicker'])
        .config(config);

})(window.angular);