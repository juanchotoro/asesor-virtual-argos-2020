(function(angular, appName) {
    'use strict';
    var app = angular.module(window.appName);
    var service = function($http, $firebaseArray, $firebaseObject, $firebaseStorage) {
        var factory = {};

        factory.enviarEmail = function(data) {
            return $http.post("https://api.appfeelingco.com/api/sendEmail", data);
        }
        factory.pardot = function(data) {
            return $http.post("https://api.appfeelingco.com/api/pardot/prospect", data);
        }
        factory.crear = function(item) {
            if (item) {
                return $firebaseArray(firebase.database().ref('/usuarios')).$add(item);
            }
            return null;
        };
        factory.subir = function(item) {
            return $firebaseStorage(firebase.storage().ref('images/' + item.name));
        };
        factory.obtener = function() {
            return $firebaseArray(firebase.database().ref('/post'));
        };
        factory.borrar = function(id) {
            $firebaseObject(firebase.database().ref('/post/' + id)).$remove();
        };
        factory.actualizar = function(item) {
            var itemUpdate = {
                titulo: item.titulo,
                contenido: item.contenido
            };
            var item = $firebaseObject(firebase.database().ref('/post/' + item.$id)).$remove();
            return $firebaseArray(firebase.database().ref('/post')).$add(itemUpdate);
        };
        factory.existeCorreo = function(email) {
            return $firebaseArray(firebase.database().ref('/usuarios').orderByChild('email').equalTo(email));
        }
        factory.obtenerAsesor = function(depto) {
            return $firebaseArray(firebase.database().ref('/asesores').orderByChild('Departamento').equalTo(depto));
        }

        return factory;
    }

    service.$inject = ["$http", "$firebaseArray", "$firebaseObject", "$firebaseStorage"];
    app.service("firebaseService", service);
})(window.angular, window.appName);